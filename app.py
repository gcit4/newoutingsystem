import cv2
import os
from flask import Flask, request, render_template, redirect, url_for
from datetime import date, datetime
import numpy as np
from sklearn.neighbors import KNeighborsClassifier
import joblib
from pymongo import MongoClient

app = Flask(__name__)

nimgs = 20
imgBackground = cv2.imread("background.png")
logo = cv2.imread("/static/images/logo.png")
datetoday = date.today().strftime("%m_%d_%y")
datetoday2 = date.today().strftime("%d-%B-%Y")

face_detector = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

# Connect to MongoDB
client = MongoClient("mongodb+srv://facerecognition:74nyf8jiRAD0obKg@cluster0.sxzmpis.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0")
db = client["facerecognition"]

def totalreg():
    return db.faces.count_documents({})

def extract_faces(img):
    try:
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        face_points = face_detector.detectMultiScale(gray, 1.2, 5, minSize=(20, 20))
        return face_points
    except:
        return []

def identify_face(facearray):
    model = joblib.load('static/face_recognition_model.pkl')
    return model.predict(facearray)

def train_model():
    faces = []
    labels = []
    userlist = os.listdir('static/faces')
    if not userlist:
        print("No images found in static/faces directory. Training aborted.")
        return
    for user in userlist:
        for imgname in os.listdir(f'static/faces/{user}'):
            img = cv2.imread(f'static/faces/{user}/{imgname}')
            resized_face = cv2.resize(img, (50, 50))
            faces.append(resized_face.ravel())
            labels.append(user)
    faces = np.array(faces)
    knn = KNeighborsClassifier(n_neighbors=5)
    knn.fit(faces, labels)
    joblib.dump(knn, 'static/face_recognition_model.pkl')


def add_record(record_type, name):
    username = name.split('_')[0]
    userid = name.split('_')[1]
    current_time = datetime.now().strftime("%H:%M:%S")

    collection = db[record_type]
    if not collection.find_one({"Roll": int(userid)}):
        record = {"Name": username, "Roll": int(userid), "Time": current_time}
        collection.insert_one(record)

def extract_records(record_type):
    try:
        client = MongoClient("mongodb+srv://facerecognition:74nyf8jiRAD0obKg@cluster0.sxzmpis.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0")
        db = client["facerecognition"]
        collection = db[record_type]

        records = collection.find()

        names = []
        rolls = []
        times = []
        l = 0

        for record in records:
            names.append(record['Name'])
            rolls.append(record['Roll'])
            times.append(record['Time'])
            l += 1

        client.close()

        return names, rolls, times, l
    except Exception as e:
        print("Error while extracting records:", e)
        return [], [], [], 0


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        if username == "Admin" and password == "Admin":
            return redirect(url_for('home'))
        else:
            return render_template('login.html', error="Invalid credentials")
    return render_template('login.html')

@app.route('/home')
def home():
    logo_path = "./images/logo.png"
    return render_template('home.html', logo_path=logo_path)

@app.route('/register')
def register():
    return render_template('register.html')

@app.route('/add', methods=['POST'])
def add():
    newusername = request.form['newusername']
    newuserid = request.form['newuserid']
    userimagefolder = 'static/faces/'+newusername+'_'+str(newuserid)
    if not os.path.isdir(userimagefolder):
        os.makedirs(userimagefolder)
    i, j = 0, 0
    cap = cv2.VideoCapture(0)
    while 1:
        _, frame = cap.read()
        faces = extract_faces(frame)
        for (x, y, w, h) in faces:
            cv2.rectangle(frame, (x, y), (x+w, y+h), (255, 0, 20), 2)
            cv2.putText(frame, f'Images Captured: {i}/{nimgs}', (30, 30),
                        cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 20), 2, cv2.LINE_AA)
            if j % 5 == 0:
                name = newusername+'_'+str(i)+'.jpg'
                cv2.imwrite(userimagefolder+'/'+name, frame[y:y+h, x:x+w])
                i += 1
            j += 1
        if i == nimgs:
            break
        cv2.imshow('Adding new User', frame)
        if cv2.waitKey(1) == 27:
            break
    cap.release()
    cv2.destroyAllWindows()
    print('Training Model')
    train_model()
    
    # Save user record to MongoDB
    db.faces.insert_one({"username": newusername, "userid": int(newuserid)})
    
    return redirect(url_for('home'))

@app.route('/recordOut', methods=['GET'])
def record_out():
    if 'face_recognition_model.pkl' not in os.listdir('static'):
        return render_template('home.html', mess='There is no trained model in the static folder. Please add a new face to continue.')

    ret = True
    cap = cv2.VideoCapture(0)
    while ret:
        ret, frame = cap.read()
        face_points = extract_faces(frame)
        if len(face_points) > 0:
            (x, y, w, h) = face_points[0]
            cv2.rectangle(frame, (x, y), (x+w, y+h), (86, 32, 251), 1)
            face = cv2.resize(frame[y:y+h, x:x+w], (50, 50))
            identified_person = identify_face(face.reshape(1, -1))[0]
            name, roll = identified_person.split('_')
            cv2.putText(frame, f'Name: {name}, ID: {roll}', (x, y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (86, 32, 251), 2)
        imgBackground[162:162 + 480, 55:55 + 640] = frame
        cv2.imshow('Record Out', imgBackground)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            add_record('RecordOut', identified_person)
            break
    cap.release()
    cv2.destroyAllWindows()
    return redirect(url_for('home'))

@app.route('/recordIn', methods=['GET'])
def record_in():
    if 'face_recognition_model.pkl' not in os.listdir('static'):
        return render_template('home.html', mess='There is no trained model in the static folder. Please add a new face to continue.')

    ret = True
    cap = cv2.VideoCapture(0)
    while ret:
        ret, frame = cap.read()
        face_points = extract_faces(frame)
        if len(face_points) > 0:
            (x, y, w, h) = face_points[0]
            cv2.rectangle(frame, (x, y), (x+w, y+h), (86, 32, 251), 1)
            face = cv2.resize(frame[y:y+h, x:x+w], (50, 50))
            identified_person = identify_face(face.reshape(1, -1))[0]
            name, roll = identified_person.split('_')
            cv2.putText(frame, f'Name: {name}, ID: {roll}', (x, y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (86, 32, 251), 2)
        imgBackground[162:162 + 480, 55:55 + 640] = frame
        cv2.imshow('Record In', imgBackground)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            add_record('RecordIn', identified_person)
            break
    cap.release()
    cv2.destroyAllWindows()
    return redirect(url_for('home'))

@app.route('/detail', methods=['GET'])
def detail():
    names_in, rolls_in, times_in, l_in = extract_records('RecordIn')
    names_out, rolls_out, times_out, l_out = extract_records('RecordOut')
    return render_template('detail.html', records_in=zip(names_in, rolls_in, times_in), records_out=zip(names_out, rolls_out, times_out))

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=int(os.environ.get('PORT', 4000)))

            



# import cv2
# import os
# from flask import Flask, request, render_template, redirect, url_for
# from datetime import date, datetime
# import numpy as np
# from sklearn.neighbors import KNeighborsClassifier
# import pandas as pd
# import joblib

# app = Flask(__name__)

# nimgs = 20
# imgBackground = cv2.imread("background.png")
# logo = cv2.imread("logo.png")
# datetoday = date.today().strftime("%m_%d_%y")
# datetoday2 = date.today().strftime("%d-%B-%Y")

# face_detector = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

# # Ensure directories exist
# if not os.path.isdir('RecordOut'):
#     os.makedirs('RecordOut')
# if not os.path.isdir('RecordIn'):
#     os.makedirs('RecordIn')
# if not os.path.isdir('static'):
#     os.makedirs('static')
# if not os.path.isdir('static/faces'):
#     os.makedirs('static/faces')

# # Initialize CSV files for today
# if f'RecordOut-{datetoday}.csv' not in os.listdir('RecordOut'):
#     with open(f'RecordOut/RecordOut-{datetoday}.csv', 'w') as f:
#         f.write('Name,Roll,Time')
# if f'RecordIn-{datetoday}.csv' not in os.listdir('RecordIn'):
#     with open(f'RecordIn/RecordIn-{datetoday}.csv', 'w') as f:
#         f.write('Name,Roll,Time')

# def totalreg():
#     return len(os.listdir('static/faces'))

# def extract_faces(img):
#     try:
#         gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#         face_points = face_detector.detectMultiScale(gray, 1.2, 5, minSize=(20, 20))
#         return face_points
#     except:
#         return []

# def identify_face(facearray):
#     model = joblib.load('static/face_recognition_model.pkl')
#     return model.predict(facearray)

# def train_model():
#     faces = []
#     labels = []
#     userlist = os.listdir('static/faces')
#     for user in userlist:
#         for imgname in os.listdir(f'static/faces/{user}'):
#             img = cv2.imread(f'static/faces/{user}/{imgname}')
#             resized_face = cv2.resize(img, (50, 50))
#             faces.append(resized_face.ravel())
#             labels.append(user)
#     faces = np.array(faces)
#     knn = KNeighborsClassifier(n_neighbors=5)
#     knn.fit(faces, labels)
#     joblib.dump(knn, 'static/face_recognition_model.pkl')

# def add_record(record_type, name):
#     username = name.split('_')[0]
#     userid = name.split('_')[1]
#     current_time = datetime.now().strftime("%H:%M:%S")

#     filepath = f'{record_type}/{record_type}-{datetoday}.csv'
#     df = pd.read_csv(filepath)
#     if int(userid) not in list(df['Roll']):
#         with open(filepath, 'a') as f:
#             f.write(f'\n{username},{userid},{current_time}')

# def extract_records(record_type):
#     df = pd.read_csv(f'{record_type}/{record_type}-{datetoday}.csv')
#     names = df['Name']
#     rolls = df['Roll']
#     times = df['Time']
#     l = len(df)
#     return names, rolls, times, l

# @app.route('/')
# def index():
#     return render_template('index.html')

# @app.route('/login', methods=['GET', 'POST'])
# def login():
#     if request.method == 'POST':
#         username = request.form['username']
#         password = request.form['password']
#         if username == "Admin" and password == "Admin":
#             return redirect(url_for('home'))
#         else:
#             return render_template('login.html', error="Invalid credentials")
#     return render_template('login.html')

# @app.route('/home')
# def home():
#     logo_path = "./images/logo.png"
#     return render_template('home.html', logo_path=logo_path)

# @app.route('/register')
# def register():
#     return render_template('register.html')

# @app.route('/add', methods=['POST'])
# def add():
#     newusername = request.form['newusername']
#     newuserid = request.form['newuserid']
#     userimagefolder = 'static/faces/'+newusername+'_'+str(newuserid)
#     if not os.path.isdir(userimagefolder):
#         os.makedirs(userimagefolder)
#     i, j = 0, 0
#     cap = cv2.VideoCapture(0)
#     while 1:
#         _, frame = cap.read()
#         faces = extract_faces(frame)
#         for (x, y, w, h) in faces:
#             cv2.rectangle(frame, (x, y), (x+w, y+h), (255, 0, 20), 2)
#             cv2.putText(frame, f'Images Captured: {i}/{nimgs}', (30, 30),
#                         cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 20), 2, cv2.LINE_AA)
#             if j % 5 == 0:
#                 name = newusername+'_'+str(i)+'.jpg'
#                 cv2.imwrite(userimagefolder+'/'+name, frame[y:y+h, x:x+w])
#                 i += 1
#             j += 1
#         if i == nimgs:
#             break
#         cv2.imshow('Adding new User', frame)
#         if cv2.waitKey(1) == 27:
#             break
#     cap.release()
#     cv2.destroyAllWindows()
#     print('Training Model')
#     train_model()
#     return redirect(url_for('home'))

# @app.route('/recordOut', methods=['GET'])
# def record_out():
#     if 'face_recognition_model.pkl' not in os.listdir('static'):
#         return render_template('home.html', mess='There is no trained model in the static folder. Please add a new face to continue.')

#     ret = True
#     cap = cv2.VideoCapture(0)
#     while ret:
#         ret, frame = cap.read()
#         face_points = extract_faces(frame)
#         if len(face_points) > 0:
#             (x, y, w, h) = face_points[0]
#             cv2.rectangle(frame, (x, y), (x+w, y+h), (86, 32, 251), 1)
#             face = cv2.resize(frame[y:y+h, x:x+w], (50, 50))
#             identified_person = identify_face(face.reshape(1, -1))[0]
#             name, roll = identified_person.split('_')
#             cv2.putText(frame, f'Name: {name}, ID: {roll}', (x, y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (86, 32, 251), 2)
#         imgBackground[162:162 + 480, 55:55 + 640] = frame
#         cv2.imshow('Record Out', imgBackground)
#         if cv2.waitKey(1) & 0xFF == ord('q'):
#             add_record('RecordOut', identified_person)
#             break
#     cap.release()
#     cv2.destroyAllWindows()
#     return redirect(url_for('home'))

# @app.route('/recordIn', methods=['GET'])
# def record_in():
#     if 'face_recognition_model.pkl' not in os.listdir('static'):
#         return render_template('home.html', mess='There is no trained model in the static folder. Please add a new face to continue.')

#     ret = True
#     cap = cv2.VideoCapture(0)
#     while ret:
#         ret, frame = cap.read()
#         face_points = extract_faces(frame)
#         if len(face_points) > 0:
#             (x, y, w, h) = face_points[0]
#             cv2.rectangle(frame, (x, y), (x+w, y+h), (86, 32, 251), 1)
#             face = cv2.resize(frame[y:y+h, x:x+w], (50, 50))
#             identified_person = identify_face(face.reshape(1, -1))[0]
#             name, roll = identified_person.split('_')
#             cv2.putText(frame, f'Name: {name}, ID: {roll}', (x, y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (86, 32, 251), 2)
#         imgBackground[162:162 + 480, 55:55 + 640] = frame
#         cv2.imshow('Record In', imgBackground)
#         if cv2.waitKey(1) & 0xFF == ord('q'):
#             add_record('RecordIn', identified_person)
#             break
#     cap.release()
#     cv2.destroyAllWindows()
#     return redirect(url_for('home'))

# @app.route('/detail', methods=['GET'])
# def detail():
#     names_in, rolls_in, times_in, l_in = extract_records('RecordIn')
#     names_out, rolls_out, times_out, l_out = extract_records('RecordOut')
#     return render_template('detail.html', records_in=zip(names_in, rolls_in, times_in), records_out=zip(names_out, rolls_out, times_out))

# if __name__ == '__main__':
#     app.run(debug=True)
